# RD LAB NODEJS HOMEWORK №2

Use NodeJS to implement CRUD API for notes.
# Endpoins:
```
User
GET:    /api​/users​/me      - Get user's profile info
DELETE: /api​/users​/me      - Delete user's profile
PATCH:  /api​/users​/me      - Change user's password
Notes
GET:    /api​/notes         - Get user's notes
POST:   /api​/notes         - Add Note for User
GET:    /api​/notes​/{id}    - Get user's note by id
PUT:    /api​/notes​/{id}    - Update user's note by id
PATCH:  /api​/notes​/{id}    - Check/uncheck user's note by id
DELETE: /api​/notes​/{id}    - Delete user's note by id
Auth:
POST:   /api​/auth​/register - Register a new user
POST:   /api​/auth​/login    - Login
```
# Models:
```
User{
  _id         string
  username    string
  password	  string
  createdDate string($date)
}

Note{
  _id         string
  userId	    string
  completed	  boolean
  text	      string
  createdDate	string($date)
}
```

# Requirements:
- Mandatory npm start script.
- Please check that your app can be launched with 'npm install' and 'npm start' commands just after git pull;
- Ability to run server on port which is defined as PORT environment variable, default to 8080;
- Use express to implement web-server;
- Use express Router for scaling your app and MVC pattern to organise project structure;
- Follow REST API rules described in Swagger file;
- Encode all users password with crypt npm package
- Use config or dotenv npm packages to store configuration for your project;
- Use jsonwebtoken package for jwt authorization;
- Use Mongo Atlas for MongoDB connection;
- Application should validate all endpoints parameters with Joi npm package;
- Server handles errors for all requests;
- Write every request info to logs;
- Application code should follow eslint rules described in .eslintrc.json file(requires eslint and eslint-config-google packages install);

# Acceptance criteria:
- Ability to register users;
- User is able to login into the system;
- User is able to view his profile info;
- User is able to view only personal notes, provide pagination parameters for notes list, request note by id;
- User is able to add, delete personal notes;
- User can check/uncheck any note;
- User can manage notes and personal profile only with valid JWT token in request;
- Gitlab repo link and project id are saved in Google spreadsheets by [link](https://docs.google.com/spreadsheets/d/1_PhsGuS_ucQO1WWaarGxdb6aYvAs6remqFLFHHVKCkY/edit?usp=sharing)

# Optional criteria:
- User can change his profile password;
- User can delete personal account;
- Ability to edit personal notes text;
- Simple UI for your application(would be a big plus).
