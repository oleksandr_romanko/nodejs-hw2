import * as express from 'express';
import { userPatchValidate } from '../middleware/validate.middleware';
import verifyToken from '../middleware/verifyToken.middleware';
import { getProfile, deleteProfile, changePassword } from '../controllers/user.controllers';

const router = express.Router();

// /api/users/me
router.get('/me', verifyToken, getProfile);

router.delete('/me', verifyToken, deleteProfile);

router.patch('/me', userPatchValidate, verifyToken, changePassword);

module.exports = router;
