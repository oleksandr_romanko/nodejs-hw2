import * as express from 'express';
import verifyToken from '../middleware/verifyToken.middleware';
import {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  patchNoteById,
  deleteNoteById,
} from '../controllers/note.controller';

const router = express.Router();

// api/notes
router.get('/', verifyToken, getNotes);

router.post('/', verifyToken, addNote);

router.get('/:id', verifyToken, getNoteById);

router.put('/:id', verifyToken, updateNoteById);

router.patch('/:id', verifyToken, patchNoteById);

router.delete('/:id', verifyToken, deleteNoteById);

module.exports = router;
