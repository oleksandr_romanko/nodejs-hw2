import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { RequestHandler, Request, Response, NextFunction } from 'express';

const userIsNotAuthorized = (res: Response) => res.status(400).json({ message: 'User is not authorized' });
const verifyToken: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization) return userIsNotAuthorized(res);
    const [, token] = [...req.headers.authorization.split(' ')];
    if (!token) return userIsNotAuthorized(res);
    req.body.user = await jwt.verify(token, config.get('jwt_secret'));
    return next();
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

export default verifyToken;
