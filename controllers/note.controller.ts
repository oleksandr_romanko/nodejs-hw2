import { RequestHandler, Request, Response } from 'express';
import NoteModel from '../models/Note';

const getNotes: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { userId } = req.body.user;
    const notes = await NoteModel.find({ userId });
    if (!notes) {
      return res.status(400).json({ message: 'Notes has no found' });
    }
    return res.json({ notes });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const addNote: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { text } = req.body;
    const { userId } = req.body.user;
    const note = new NoteModel({ text, userId });
    await note.save();
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const getNoteById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { userId } = req.body.user;
    const note = await NoteModel.find({ _id: id, userId });
    if (!note) {
      return res.status(400).json({ message: 'Note has no found' });
    }
    return res.json({ note });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const updateNoteById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { text } = req.body;
    const { id } = req.params;
    const { userId } = req.body.user;
    const note = await NoteModel.findOneAndUpdate({ userId, _id: id }, { text });
    if (!note) {
      return res.status(400).json({ message: 'Note has no found' });
    }
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const patchNoteById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { userId } = req.body.user;
    const note = await NoteModel.findOne({ userId, _id: id });
    let update;
    if (note && 'completed' in note) {
      update = await NoteModel.findOneAndUpdate(
        { userId, _id: id },
        { completed: !note.completed },
      );
    }
    if (!update) {
      return res.status(400).json({ message: 'Note has no found' });
    }
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

const deleteNoteById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const { userId } = req.body.user;
    const note = await NoteModel.findOneAndDelete({ _id: id, userId });
    if (!note) {
      return res.status(400).json({ message: 'Note has no found' });
    }
    return res.json({ message: 'Succses' });
  } catch (e) {
    return res.status(500).json({ message: 'Server error' });
  }
};

export {
  getNotes, addNote, getNoteById, updateNoteById, patchNoteById, deleteNoteById,
};
